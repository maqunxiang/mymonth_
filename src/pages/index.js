import React from 'react'

import RouterView from "../route/routerView";
import {NavLink} from "react-router-dom"

const index = (props)=>{
    let {routeList,navlink}=props;
    return (
        <div>
            <div className="homeBottom">
             {
                 navlink&&navlink.length>0?navlink.map((item,index)=>{
                     return <NavLink to={item.path } key={index}>{item.name}</NavLink>
                 }):null
             }
           </div>
           <div className="homeTop">
               <RouterView routeList={routeList}/>
           </div>
          
        </div>
    )
}



export default index