
import './App.css';
import RouterView from './route/routerView';
import RouterList from "./route/routerList";
import { BrowserRouter as Router } from "react-router-dom"

function App() {
  return (
    <Router>
       <div className="App">
       <RouterView routeList={RouterList}></RouterView>
    </div>
    </Router>
   
  );
}

export default App;
