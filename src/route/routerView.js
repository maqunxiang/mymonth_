import { Switch, Route, Redirect } from "react-router-dom";
import { Suspense } from "react";


const RouterView= ({ routeList }) => {
  //路径
  let list =
    routeList && routeList.length > 0
      ? routeList.filter((item) => !item.to)
      : null;
  //重定向
  let redirect =
    routeList && routeList.length > 0
      ? routeList.filter((item) => item.to)[0]
      : null;
  return <Suspense fallback="loading">
       <Switch>
           {
               //路径
               list&&list.length>0?list.map((item,index)=>{
                   return <Route path={item.path} key={index} render={(routeProps)=>{
                       let Com=item.component;
                       //判断是否需要调到登录页
                       if(item.flag&&!localStorage.getItem("login")){
                           return <Redirect to="/login" from={item.path}/>
                       }
                       if(item.component){
                           return <Com {...routeProps} routeList={item.children} navlink={item.children?.filter(item2=>!item2.to)}/>
                       }
                       return <Com {...routeProps}></Com>
                   }}/>
               }):null
           }
           {
               //重定向
               redirect&&<Redirect to={redirect.to} from={redirect.from} />
           }
       </Switch>
  </Suspense>;
};
export default RouterView;