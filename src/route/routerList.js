import { lazy } from "react";

let routeList = [
     //一级重定向
     {
          from: "/",
          to: "/home"
     },
     //home页
     {
          path: "/home",
          component: lazy(() => import("../pages/index")),
          children: [
               {
                    from: "/home",
                    to: "/home/home"
               },
               {
                    path: "/home/home",
                    name: "家",
                    component: lazy(() => import("../pages/home/home")),
               },
               {
                    path: "/home/about",
                    name: "关于我们",
                    component: lazy(() => import("../pages/home/about")),
               },
               {
                    path: "/home/server",
                    name: "商业与服务",
                    component: lazy(() => import("../pages/home/server")),
               },
               {
                    path: "/home/news",
                    name: "新闻中心",
                      component: lazy(() => import("../pages/home/news")),
               },
               {
                    path: "/home/touzi",
                    name: "投资者关系",
                    component: lazy(() => import("../pages/home/touzi")),
               },
               {
                    path: "/home/our",
                    name: "投资我们",
                component: lazy(() => import("../pages/home/our")),
               },
               {
                    path: "/home/join",
                    name: "加入我们",
                    component: lazy(() => import("../pages/home/join")),
               },
          ]
     },

];

export default routeList;




































    // //详情
    // {
    //     path:"/deatils/:id",
    //     name:"详情页",
    //     component:Details
    // },
    // //地址页
    // {
    //     path:"/Addr",
    //     name:"地址页",
    //     component:Addr
    // },
    // //新增地址页
    // {
    //     path:"/addrAdd",
    //     name:"新增页",
    //     component:AddrAddr
    // },
    // //修改地址页
    // {
    //     path:"/EmitAddr",
    //     name:"修改地址页",
    //     component:EmitAddr
    // },
    // //登录页
    // {
    //     path:"/login",
    //     name:"登录页",
    //     component:Login
    // }